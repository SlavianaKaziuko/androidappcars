package com.example.cars.ui.carsList;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cars.R;
import com.example.cars.ui.base.BaseViewHolder;

import java.util.List;

public class CarsRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private final CarItemListener carItemClickListener;
    private final List<CarsListItemModel> items;

    public CarsRecyclerViewAdapter(List<CarsListItemModel> cars, CarItemListener listener) {
        carItemClickListener = listener;
        items = cars;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType().ordinal();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        switch (CarsListItemTypes.values()[viewType]){
            case CAR:
                return new CarViewHolder(layoutInflater.inflate(R.layout.item_car_details, viewGroup, false), carItemClickListener);
            case BRAND:
                return new CarBrandViewHolder(layoutInflater.inflate(R.layout.item_car_brand, viewGroup, false));
            case PROGRESS:
                return new ProgressViewHolder(layoutInflater.inflate(R.layout.item_progress, viewGroup, false));
            case EMPTY:
                return new EmptyViewHolder(layoutInflater.inflate(R.layout.item_empty, viewGroup, false));

        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int i) {
        if (i >= 0 && i < items.size()) {
            CarsListItemModel itemModel = items.get(i);
            viewHolder.bind(itemModel);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface CarItemListener {
        public void onCarItemClicked(int index);
    }
}
