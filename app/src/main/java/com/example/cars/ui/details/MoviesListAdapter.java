package com.example.cars.ui.details;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cars.R;
import com.example.cars.ui.base.BaseViewHolder;

import java.util.ArrayList;

public class MoviesListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ArrayList<String> movies;

    public MoviesListAdapter(ArrayList<String> movies) {
        this.movies = movies;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        return new ItemMovieViewHolder(layoutInflater.inflate(R.layout.item_movie_name, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int position) {
        if (position >= 0 && position < movies.size()) {
            viewHolder.bind(movies.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
