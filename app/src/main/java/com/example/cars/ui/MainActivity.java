package com.example.cars.ui;

import android.os.Bundle;
import com.example.cars.ui.details.DetailsFragment;
import com.example.cars.R;
import com.example.cars.ui.carsList.CarsFragment;
import com.example.cars.data.Car;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements CarsFragment.OnFragmentInteractionListener, DetailsFragment.OnFragmentInteractionListener {
    CarsFragment.OnFragmentInteractionListener callbackCars;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayFragment();

    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

    }

    public void displayFragment() {
        CarsFragment simpleFragment = new CarsFragment();
        // Get the FragmentManager and start a transaction.
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.action_container, simpleFragment).commit();

    }

    public void onFragmentInteraction(Car car) {

    }

    public void onFragmentInteraction(String callback) {
        System.out.println(callback);
    }

}
