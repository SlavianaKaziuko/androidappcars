package com.example.cars.ui.carsList;

import android.view.View;
import android.widget.TextView;
import com.example.cars.R;
import com.example.cars.ui.base.BaseViewHolder;

public class CarBrandViewHolder extends BaseViewHolder<CarsListItemModel> {
    final TextView textCarBrandName;

    CarBrandViewHolder(View view) {
        super(view);
        textCarBrandName = view.findViewById(R.id.textBrandName);
    }

    @Override
    public void bind(CarsListItemModel data) {
        textCarBrandName.setText(data.getName() != null ? data.getName() : "");
    }
}
