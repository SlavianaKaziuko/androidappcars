package com.example.cars.ui.details;

import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.cars.R;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.cars.data.Car;
import com.example.cars.data.CarsData;
import com.example.cars.ui.carsList.CarsListItemModel;
import kotlin.Unit;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class DetailsFragment extends Fragment implements CarsData.CallbackCarsData {
    private static final String ARG_PARAM_INDEX = "mParamCarIndex";

    private TextView textName;
    private TextView textModel;
    private TextView textYear;
    private TextView labelMovie;
    private RecyclerView recyclerViewMovies;
    private ImageView imageCar;

    private Car carData = new Car();
    private ArrayList<String> carMovies = new ArrayList<>();
    private CarsData carsDataModel;
    private int mParamCarIndex;
    private OnFragmentInteractionListener mListener;
    private MoviesListAdapter moviesListAdapter;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(int param) {

        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_INDEX, param);

        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamCarIndex = getArguments().getInt(ARG_PARAM_INDEX);
            carsDataModel = new CarsData(this);
            carsDataModel.getCarsData(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        imageCar = view.findViewById(R.id.imageCar);
        textName = view.findViewById(R.id.textCarName);
        textModel = view.findViewById(R.id.textCarModel);
        textYear = view.findViewById(R.id.textCarYear);
        labelMovie = view.findViewById(R.id.labelCarMovie);
        recyclerViewMovies = view.findViewById(R.id.recyclerViewMovies);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerViewMovies.setLayoutManager(layoutManager);
        moviesListAdapter = new MoviesListAdapter(carMovies);
        recyclerViewMovies.setAdapter(moviesListAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d("Details_Fragment", "onDetach");
        super.onDetach();
        mListener = null;
    }

    public void fillData() {

        Glide.with(this).load(carData.getImageUrl()).into(imageCar);

        textName.setText(carData.getName());
        textModel.setText(carData.getModel());
        textYear.setText(String.valueOf(carData.getYear()));
        if (carMovies.size() == 0) {
            labelMovie.setVisibility(View.GONE);
        } else {
            labelMovie.setVisibility(View.VISIBLE);
            moviesListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void callingBack(@NotNull ArrayList<CarsListItemModel> items) {
        CarsData.Companion.getCarById(mParamCarIndex, (Car car) -> {
            this.carData = car;
            if (carData != null) {
                if (carData.getMovies() != null) {
                    carMovies.addAll(carData.getMovies());
                }

                fillData();
            }
            return Unit.INSTANCE;
        });

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String callback);
    }


    //For Tests
    @Override
    public void onStart() {
        Log.d("Details_Fragment", "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d("Details_Fragment", "onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.d("Details_Fragment", "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d("Details_Fragment", "onDestroy");
        super.onDestroy();
    }
}