package com.example.cars.data

import android.util.Log
import com.example.cars.ui.carsList.CarsListItemTypes
import com.example.cars.ui.carsList.CarsListItemModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

class CarsData(internal var callback: CallbackCarsData) {

    companion object {
        fun getCarById(id: Int, listener: (Car) -> Unit) {
            NetworkService.getInstance().carsDataApi.cars.enqueue(object : Callback<ResponseData> {
                override fun onResponse(call: Call<ResponseData>, response: Response<ResponseData>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            listener(response.body()!!.data.first { c -> c.getId() == id })
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseData>, t: Throwable) {
                    Log.d("CARS_Network", t.localizedMessage)
                }
            })
        }

        fun instance(callback: CallbackCarsData) : CarsData {
            return CarsData(callback)
        }
    }

    fun getCarsData(isFirstFetch: Boolean = true) {
        NetworkService.getInstance().carsDataApi.cars.enqueue(object : Callback<ResponseData> {
            override fun onResponse(call: Call<ResponseData>, response: Response<ResponseData>) {
                if (response.isSuccessful) {
                    var cars = ArrayList<Car>()
                    cars.addAll(response.body()!!.data)
                    var carsByBrand = ArrayList<CarsListItemModel>()

                    if (isFirstFetch) {
                        var temp = ArrayList<CarsListItemModel>()
                        temp.addAll(cars.map { car -> CarsListItemModel(CarsListItemTypes.CAR, car.brand, car) })
                        temp.addAll(cars.map { c -> CarsListItemModel(CarsListItemTypes.BRAND, c.brand) }.distinctBy { c -> c.name })
                        carsByBrand.addAll(temp.sorted())

                        carsByBrand.add(CarsListItemModel(CarsListItemTypes.PROGRESS))
                    } else {
                        carsByBrand.add(CarsListItemModel(CarsListItemTypes.EMPTY))
                    }

                    callback.callingBack(carsByBrand)
                }
            }

            override fun onFailure(call: Call<ResponseData>, t: Throwable) {
                Log.d("CARS_Network", t.localizedMessage)
            }
        })
    }

    interface CallbackCarsData {
        fun callingBack(items: ArrayList<CarsListItemModel>)
    }
}

/*
        cars.add(Car("Doc Brown's DeLorean","DeLorean", "DMC-12", "http://en.bcdn.biz/Images/2016/6/16/e82a748c-cc0e-47d8-ac78-5a0ca0e9553a.jpg", 1981, arrayListOf("Back to the Future")))
        cars.add(Car("Herbie","Volkswagen", "Bug (Beetle)", "http://en.bcdn.biz/Images/2016/6/16/0c536804-7f82-4bb5-9bef-08c946ba183b.jpg", 1963, arrayListOf("Herbie series")))
        cars.add(Car("The General Lee","Dodge", "Charger", "http://en.bcdn.biz/Images/2016/6/16/110a08f4-5a98-4a1f-bbfb-dc16dccca890.jpg", 1969, arrayListOf("Dukes of Hazzard")))
        cars.add(Car("Bullitt","Ford", "Mustang GT390 Fastback", "http://en.bcdn.biz/Images/2016/6/16/e1e3baa3-5075-4561-ae57-a3bff3b4e8fe.jpg", 1968, arrayListOf("Bullitt")))
        cars.add(Car("Ecto-1","Cadillac", "Miller-Meteor", "http://en.bcdn.biz/Images/2016/6/16/dfdace51-92e2-4ace-be0b-6f66e77d0daa.jpg", 1959, arrayListOf("Ghostbusters")))
        cars.add(Car("Bandit's Trans Am","Pontiac", "Trans Am", "http://en.bcdn.biz/Images/2016/6/16/b7558152-5bc6-4de8-8686-c407b01f11dd.jpg", 1977, arrayListOf("Smokey and the Bandit")))
        cars.add(Car("Starsky's Gran Torino","Ford", "Gran Torino", "http://en.bcdn.biz/Images/2016/6/16/6edbaa3b-4d54-486f-ae84-09aee6b11dad.jpg", 1963, arrayListOf("Starsky & Hutch")))
 */