package com.example.cars.ui.carsList;

public enum CarsListItemTypes {
    BRAND,
    CAR,
    PROGRESS,
    EMPTY
}
