package com.example.cars.ui.carsList;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.example.cars.R;
import com.example.cars.data.Car;
import com.example.cars.ui.base.BaseViewHolder;

public class CarViewHolder extends BaseViewHolder<CarsListItemModel> {
    int itemId = -1;

    final ImageView imageViewCar;
    final TextView textName;
    final TextView textModel;

    CarViewHolder(@NonNull View view) {
        super(view);
        imageViewCar = view.findViewById(R.id.imageCar);
        textName = view.findViewById(R.id.textName);
        textModel = view.findViewById(R.id.textModel);
    }

    CarViewHolder(@NonNull View view, final CarsRecyclerViewAdapter.CarItemListener listener) {
        this(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemId >= 0) {
                    listener.onCarItemClicked(itemId);
                }
            }
        });
    }

    @Override
    public void bind(CarsListItemModel data) {
        Car car = data.getCar();
        itemId = car.getId();
        textName.setText(car.getName());
        textModel.setText(car.getModel());

        if (!car.getImageUrl().isEmpty()) {
            Glide.with(imageViewCar.getContext())
                    .load(car.getImageUrl())
                    .into(imageViewCar);
        } else {
            Glide.with(imageViewCar.getContext()).clear(imageViewCar);
        }
    }
}
