package com.example.cars.ui.carsList

import com.example.cars.data.Car

class CarsListItemModel (
    var type: CarsListItemTypes,
    val name: String = "",
    private var _car: Car? = null
) : Comparable <CarsListItemModel> {

    fun setCarValue(value: Car) {
        this._car = value
        if (_car == null) CarsListItemTypes.BRAND else CarsListItemTypes.CAR
    }

    fun getCar() : Car? {
        return this._car
    }


    override fun compareTo(other: CarsListItemModel): Int {
        var result = this.name.compareTo(other.name)
        if (result == 0) {
            if (this.getCar() == null && other.getCar() != null) {
                result = -1
            } else if (this.getCar() != null && other.getCar() == null) {
                result = 1
            } else if (this.getCar() != null && other.getCar() != null) {
                result = (this.getCar()!!.name).compareTo(other.getCar()!!.name)
            }
        }

        return result;
    }

}
