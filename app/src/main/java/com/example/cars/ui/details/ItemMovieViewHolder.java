package com.example.cars.ui.details;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.example.cars.R;
import com.example.cars.ui.base.BaseViewHolder;

public class ItemMovieViewHolder extends BaseViewHolder<String> {
    final TextView textMovieName;

    public ItemMovieViewHolder(@NonNull View itemView) {
        super(itemView);
        textMovieName = itemView.findViewById(R.id.textMovieName);
    }

    @Override
    public void bind(String data) {
        textMovieName.setText(data);

    }
}
