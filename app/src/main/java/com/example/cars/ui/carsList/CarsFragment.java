package com.example.cars.ui.carsList;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cars.ui.details.DetailsFragment;
import com.example.cars.R;
import com.example.cars.data.Car;
import com.example.cars.data.CarsData;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**

 */
public class CarsFragment extends Fragment implements CarsRecyclerViewAdapter.CarItemListener, CarsData.CallbackCarsData {
    private static final String ARG_PARAM_SCROLL_Y = "scrollY";

    private RecyclerView carsRecyclerView;
    private ArrayList<CarsListItemModel> items = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    private CarsRecyclerViewAdapter listAdapter;
    private RecyclerView.OnScrollListener onScrollListener;
    private int scrollY;

    private boolean loading = false;
    private boolean somethingToLoad = true;

    public static CarsFragment newInstance() {
        Bundle arg = new Bundle();
        arg.putInt(ARG_PARAM_SCROLL_Y, 0);

        CarsFragment fragment = new CarsFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    public CarsFragment () {
        Log.d("Cars_Fragment", "CarsFragment");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("Cars_Fragment", "onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            scrollY = getArguments().getInt(ARG_PARAM_SCROLL_Y);
            Log.d("Cars_Fragment", "scrolX = " + String.valueOf(scrollY));
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("Cars_Fragment", "onCreateView");

        if (getArguments() != null) {
            scrollY = getArguments().getInt(ARG_PARAM_SCROLL_Y);
            Log.d("Cars_Fragment", "scrolX = " + String.valueOf(scrollY));
        }

        View view = inflater.inflate(R.layout.fragment_cars, container, false);
        carsRecyclerView = view.findViewById(R.id.recyclerViewCars);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        carsRecyclerView.setLayoutManager(layoutManager);

        listAdapter = new CarsRecyclerViewAdapter(items, this);
        carsRecyclerView.setAdapter(listAdapter);
        if (somethingToLoad) loadData();

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrollY += dy;

                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!loading && somethingToLoad && totalItemCount <= lastVisibleItem + 1) {
                    somethingToLoad = false;
                    loadData();
                }
            }
        };
        carsRecyclerView.addOnScrollListener(onScrollListener);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        Log.d("Cars_Fragment", "onAttach");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d("Cars_Fragment", "onDetach");
        super.onDetach();
        mListener = null;
        onScrollListener = null;
    }

    public void onCarItemClicked(int id) {
        DetailsFragment detailsFragment = DetailsFragment.newInstance(id);
        FragmentTransaction fragmentTransaction = this.getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.action_container, detailsFragment).addToBackStack("ShowDetails").commit();

    }

    //Data loading
    private void loadData() {
        if (items.size() > 1) items.remove(items.size() - 1);
        CarsData.Companion.instance(this).getCarsData(somethingToLoad);
        loading = true;
    }

    @Override
    public void callingBack(@NotNull ArrayList<CarsListItemModel> items) {
        loading = false;
        this.items.addAll(items);

        listAdapter.notifyDataSetChanged();
    }

    /**
     * This interface must be implemented by activities that contain this
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Car car);
    }


    //For Tests
    @Override
    public void onStart() {
        Log.d("Cars_Fragment", "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d("Cars_Fragment", "onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.d("Cars_Fragment", "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d("Cars_Fragment", "onDestroy");
        super.onDestroy();
    }
}