package com.example.cars.ui.carsList;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.cars.ui.base.BaseViewHolder;

public class EmptyViewHolder extends BaseViewHolder {
    public EmptyViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    @Override
    public void bind(Object data) {

    }
}
