package com.example.cars.data
import android.os.Parcel
import android.os.Parcelable
import java.lang.StringBuilder

class Car (
    val name: String = "",
    val brand: String = "",
    val model: String = "",
    val imageUrl: String = "",
    val year: Int = 0,
    val movies: ArrayList<String> = ArrayList()
) : Parcelable {


    fun getId() : Int {
        return name.toCharArray().sumBy { c -> c.toInt() }
    }

    fun getMoviesInString() : String {
        if (!movies.isEmpty()) {
            val sb = StringBuilder()
            for (movie in movies.iterator()) {
                sb.append(movie + "\n")
            }

            return sb.toString()
        }

        return ""
    }

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        arrayListOf<String>().apply {
            parcel.readList(this, String::class.java.classLoader)
        }
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(brand)
        parcel.writeString(model)
        parcel.writeString(imageUrl)
        parcel.writeInt(year)
        parcel.writeList(movies)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Car> {
        override fun createFromParcel(parcel: Parcel): Car {
            return Car(parcel)
        }

        override fun newArray(size: Int): Array<Car?> {
            return arrayOfNulls(size)
        }
    }

}
